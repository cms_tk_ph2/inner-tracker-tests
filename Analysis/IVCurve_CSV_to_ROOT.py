import array
import ROOT

def IVCurve_CSV_to_ROOT(
                        moduleName, # This name will be specific to TFPX / TBPX / TEPX conventions
                        rootPath,   # This should be in the format of "Detector/Board_0/OpticalGroup_0/Hybrid_0"
                                    # The Board, Optical Group and Hybrid numbers should be found and passed by OSU-GUI / Dirigent
                        inputCSV,    # Filename outputted by the IV-Curve script
                        outputDir   # Output directory
                        ):
  file = open(inputCSV)
  line = file.readlines()
  v_Voltage = [-1.*float(i) for i in line[0].strip().split(",")]; v_Voltage = array.array('d', v_Voltage)
  v_Current = [-1e6*float(i) for i in line[1].strip().split(",")]; v_Current = array.array('d', v_Current)
  file.close()

  g_IV = ROOT.TGraph(len(v_Voltage), v_Voltage, v_Current)
  g_IV.SetTitle("; Reverse Bias (V); Leakage Current (#muA)")
  g_IV.SetName("g_IV")
  g_IV.SetMarkerStyle(8)
  g_IV.SetMaximum(max(v_Current)*1.20)
  g_IV.SetMinimum(0)
  c_IV = ROOT.TCanvas("c_IV", "c_IV", 800, 800)
  g_IV.Draw()

  file = ROOT.TFile(outputDir+"/Result_RunIVCurve_"+moduleName+".root", "RECREATE")
  file.mkdir(rootPath)
  file.cd(rootPath)
  c_IV.Write("c_IV")
  file.Close()
