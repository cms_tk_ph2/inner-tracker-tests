RegisterSettings = {
"user.ctrl_regs.fast_cmd_reg_2.trigger_source"      : 2, 
"user.ctrl_regs.fast_cmd_reg_2.HitOr_enable_l12"    : 0,
"user.ctrl_regs.ext_tlu_reg1.dio5_ch1_thr"          : 128,
"user.ctrl_regs.ext_tlu_reg1.dio5_ch2_thr"          : 40,
"user.ctrl_regs.ext_tlu_reg2.dio5_ch3_thr"          : 128,
"user.ctrl_regs.ext_tlu_reg2.dio5_ch4_thr"          : 128,
"user.ctrl_regs.ext_tlu_reg2.dio5_ch5_thr"          : 128,
"user.ctrl_regs.ext_tlu_reg2.tlu_delay"             : 0,
"user.ctrl_regs.reset_reg.nb_of_pll_words"          : 2,
"user.ctrl_regs.reset_reg.nb_of_sync_words"         : 2,
"user.ctrl_regs.reset_reg.ext_clk_en"               : 0,
"user.ctrl_regs.fast_cmd_reg_3.triggers_to_accept"  : 10,
"user.ctrl_regs.fast_cmd_reg_7.autozero_freq"       : 1000,
"user.ctrl_regs.gtx_rx_polarity.fmc_l12"            : 0,
"user.ctrl_regs.gtx_rx_polarity.fmc_l8"             : "0xbb",
"user.ctrl_regs.Aurora_block.self_trigger_en"       : 0,
"user.ctrl_regs.cnf_cmd_ctrl.nb_slow_cmd_before_sync": 0,
"user.ctrl_regs.cnf_cmd_ctrl.nb_slow_cmd_before_pll" : 0,
}
