# B. Raciti: 2024/07/10 - bianca.raciti@cern.ch
import os
import re
import argparse
from ast import literal_eval

parser = argparse.ArgumentParser(description='Tuning and missing-bumps identification with crosstalk method')
parser.add_argument('-dut_type','--dut_type', help  = 'DUT: quad or scc', default = 'scc', type = str)
args = parser.parse_args()

def replace_xml(file_path, keywords, new_numbers):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    for keyword, new_number in zip(keywords, new_numbers):
        pattern = re.compile(rf'({re.escape(keyword)}[^0-9]*)(-?\d+(\.\d+)?([eE][+-]?\d+)?)')
        for i, line in enumerate(lines):
            match = pattern.search(line)
            if match:
                old_number = match.group(2)
                new_line = line.replace(old_number, str(new_number))
                lines[i] = new_line
    with open(file_path, 'w') as file:
        file.writelines(lines)

def replace_xml_string(file_path, keywords, new_strings):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    for keyword, new_string in zip(keywords, new_strings):
        pattern = re.compile(rf'({re.escape(keyword)}[^>]*>)([^<]+)(</Setting>)')
        for i, line in enumerate(lines):
            match = pattern.search(line)
            if match:
                old_string = match.group(2).strip()
                new_line = line.replace(old_string, new_string)
                lines[i] = new_line
    with open(file_path, 'w') as file:
        file.writelines(lines)

def read_txt(file_path,parameters):
    values = []
    with open(file_path,'r') as file:
        data = file.read()
    for i, name in enumerate(parameters):
        check_val = re.findall(str(name)+'.*', data)
        values.append(literal_eval(check_val[0].split()[3]))
    return values

def tune(xmlfile, txtfile):

    replace_xml(xmlfile,\
    ['CAL_EDGE_FINE_DELAY','SEL_CAL_RANGE','VCAL_HIGH','TriggerConfig','trigger_source','tlu_delay','triggers_to_accept',\
    'nEvents','nEvtsBurst','nTRIGxEvent','INJtype','ResetMask',\
    'ROWstart','ROWstop','COLstart','COLstop',\
    'LatencyStart','LatencyStop',\
    'VCalHstart','VCalHstop','VCalHnsteps','VCalMED',\
    'DoNSteps','ThrStart','ThrStop','TargetThr','OccPerPixel','DoOnlyNGroups','nClkDelays'],
    [18,1,1300,132,2,0,10,   100,100,10,1,0,   0,335,0,431,   130,134,\
    100,500,25,100,   0,400,500,2000,   '1e-4',0,400])

    print('********************** Threshold adjustment 2 ke- **********************\n')
    replace_xml(xmlfile,\
    ['nTRIGxEvent','INJtype','DoOnlyNGroups',\
    'TargetThr','ResetMask'],
    [10,1,0,   2000,1])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c thradj')
    values = read_txt(txtfile, ['DAC_GDAC_L_LIN','DAC_GDAC_R_LIN','DAC_GDAC_M_LIN'])
    replace_xml(xmlfile, ['DAC_GDAC_L_LIN','DAC_GDAC_R_LIN','DAC_GDAC_M_LIN'], values)

    print('********************** Threshold equalization 2 ke- **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','VCalHnsteps','DoNSteps'],
    [400,400,40,0])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c threqu')

    print('********************** Noise scan **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype','ResetMask'],
    [int(2e5),int(1e4),10,0,0])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c noise')

    print('********************** Threshold adjustment 1.5 ke- **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype','VCalHstop','DoOnlyNGroups',\
    'TargetThr'],
    [100,100,   10,1,400,0,   1500])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c thradj')
    values = read_txt(txtfile, ['DAC_GDAC_L_LIN','DAC_GDAC_R_LIN','DAC_GDAC_M_LIN'])
    replace_xml(xmlfile, ['DAC_GDAC_L_LIN','DAC_GDAC_R_LIN','DAC_GDAC_M_LIN'], values)

    print('********************** Threshold equalization 1.5 ke- **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','VCalHnsteps','DoNSteps'],
    [400,400,40,1])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c threqu')

    print('********************** Noise scan **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype'],
    [int(2e5),int(1e4),10,0])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c noise')

    print('********************** Injection delay **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype','DoOnlyNGroups'],
    [100,100,1,1,1])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c injdelay')
    values = read_txt(txtfile, ['CAL_EDGE_FINE_DELAY'])
    replace_xml(xmlfile, ['CAL_EDGE_FINE_DELAY'], [values[0]]) # test 21 May 2024

    print('********************** Threshold adjustment 1.2 ke- **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype','VCalHstop','DoOnlyNGroups',\
    'TargetThr'],
    [100,100,   10,1,350,0,   1200])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c thradj')
    values = read_txt(txtfile, ['DAC_GDAC_L_LIN','DAC_GDAC_R_LIN','DAC_GDAC_M_LIN'])
    replace_xml(xmlfile, ['DAC_GDAC_L_LIN','DAC_GDAC_R_LIN','DAC_GDAC_M_LIN'], values)

    print('********************** Noise scan **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype'],
    [int(2e5),int(1e4),10,0])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c noise')
    
    print('********************** Threshold scan **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype','VCalHnsteps','DoOnlyNGroups'],
    [100,100,10,1,25,0])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c scurve')

    print('********************** Gain scan **********************\n')
    replace_xml(xmlfile,\
    ['VCalHstop','VCalHnsteps'],
    [4000,40])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c gain')

    print('********************** Pixelalive scan **********************\n')
    replace_xml(xmlfile,\
    ['VCAL_HIGH','nEvents','nEvtsBurst','nTRIGxEvent','INJtype'],
    [400,100,100,10,1])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c pixelalive')

def missingbumps_xt(xmlfile, txtfile):
    
    print('********************** Tornado plot coupled **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype','ResetMask','DoOnlyNGroups',\
    'StartValueDAC1','StopValueDAC1','StepDAC1',\
    'StartValueDAC2','StopValueDAC2','StepDAC2'],
    [100,100,1,5,1,1,\
    0,63,1,\
    3800,4000,20])
    replace_xml_string(xmlfile,['RegNameDAC1','RegNameDAC2'],['CAL_EDGE_FINE_DELAY','VCAL_HIGH'])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c gendacdac')

    print('********************** Tornado plot uncoupled **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype','ResetMask','DoOnlyNGroups',\
    'StartValueDAC1','StopValueDAC1','StepDAC1',\
    'StartValueDAC2','StopValueDAC2','StepDAC2'],
    [100,100,1,6,1,1,\
    0,63,1,\
    3800,4000,20])
    replace_xml_string(xmlfile,['RegNameDAC1','RegNameDAC2'],['CAL_EDGE_FINE_DELAY','VCAL_HIGH'])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c gendacdac')

    print('********************** Injection delay: coupled **********************\n')
    replace_xml(xmlfile,\
    ['VCAL_HIGH','nEvents','nEvtsBurst','nTRIGxEvent','INJtype','DoOnlyNGroups'],
    [4000,   100,100,1,5,1])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c injdelay')
    values = read_txt(txtfile, ['CAL_EDGE_FINE_DELAY'])
    replace_xml(xmlfile, ['CAL_EDGE_FINE_DELAY'], [values[0]+2]) # chin-chia and bianca found out about this on 10th Jan 2024: ask them!
    
    print('********************** Pixelalive scan: coupled pixels **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype','ResetMask','DoOnlyNGroups'],
    [300,300,10,5,1,0])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c pixelalive')

    print('********************** Injection delay: uncoupled **********************\n')
    replace_xml(xmlfile,\
    ['VCAL_HIGH','nEvents','nEvtsBurst','nTRIGxEvent','INJtype','DoOnlyNGroups'],
    [4000,   100,100,1,6,1])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c injdelay')
    values = read_txt(txtfile, ['CAL_EDGE_FINE_DELAY'])
    replace_xml(xmlfile, ['CAL_EDGE_FINE_DELAY'], [values[0]+4]) # see above.

    print('********************** Pixelalive scan: uncoupled pixels **********************\n')
    replace_xml(xmlfile,\
    ['nEvents','nEvtsBurst','nTRIGxEvent','INJtype','ResetMask','DoOnlyNGroups'],
    [300,300,10,6,1,0])
    os.system('CMSITminiDAQ -f '+xmlfile+' -c pixelalive')

def main(args):

    dut_type = args.dut_type

    if dut_type == 'quad':
        xmlfile = ['CMSIT_RD53B_15.xml','CMSIT_RD53B_14.xml','CMSIT_RD53B_13.xml','CMSIT_RD53B_12.xml']
        txtfile = ['CMSIT_RD53B_15.txt','CMSIT_RD53B_14.txt','CMSIT_RD53B_13.txt','CMSIT_RD53B_12.txt']

        # tune chips individually
        for i in range(len(xmlfile)):
            tune(xmlfile[i], txtfile[i])
        # missing bumps, also individually
        for i in range(len(xmlfile)):
            missingbumps_xt(xmlfile[i], txtfile[i])
            
    else:
        xmlfile = 'CMSIT_RD53B.xml'
        txtfile = 'CMSIT_RD53B.txt'
        
        tune(xmlfile, txtfile)
        missingbumps_xt(xmlfile, txtfile)    

if __name__ == "__main__":
    main(args)
