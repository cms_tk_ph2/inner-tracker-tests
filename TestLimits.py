'''
These are currently example limits to demonstrate how such an object might work. 
Using the name of the test from inner-tracker-tests/TestSequences.py, one can 
extract an agreed upon acceptable range for results from a particular test.

Example usage:
from TestLimits import Limits
test = Limits["PixelAlive"]["total_masked"]["max"]
'''

Limits = {
    # Currently arbitrary
    "PixelAlive" : {
        "total_masked"      : {"min": 0,    "max": 10000}
    },
    # Currently arbitrary. TDAC_mean should be around 16
    "ThresholdEqualization" : {
        "TDAC_mean"         : {"min": 14,   "max": 18},
        "TDAC_width"        : {"min": 0,    "max": 8}
    },
    # Plus/minus 10 % (e.g. 2000 * 1.1 = 2200, 2200 / 5 = 440)
    "ThresholdAdjustment_2000" : {
        "GDAC_value"        : {"min": 360,  "max": 440}
    },
    # TODO # Currently arbitrary
    "SCurveScan" : {
        "threshold_mean"    : {"min": 100,  "max": 1000},
        "threshold_width"   : {"min": 0,    "max": 200},
        "noise_mean"        : {"min": 0,    "max": 60},
        "noise_width"       : {"min": 0,    "max": 20}
    },
    # Plus/minus 10 %
    "ThresholdAdjustment_1200" : {
        "GDAC_value"        : {"min": 216,  "max": 264}
    },
    # Plus/minus 10 %
    "ThresholdAdjustment_1000" : {
        "GDAC_value"        : {"min": 180,  "max": 220}
    }
}
