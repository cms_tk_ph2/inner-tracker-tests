import sys
import os
import copy

hw_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(hw_dir)
import HWSettings

globalSettingsA = {
'EN_CORE_COL_SYNC':     "0",
'EN_CORE_COL_LIN_1': "65535",
'EN_CORE_COL_LIN_2':     "1",
'EN_CORE_COL_DIFF_1':     "0",
'EN_CORE_COL_DIFF_2':     "0",
 
'EN_CORE_COL_CAL_LIN_1': "65535",
'EN_CORE_COL_CAL_LIN_2': "65535",
'EN_CORE_COL_CAL_LIN_3': "65535",
'EN_CORE_COL_CAL_LIN_4': "65535",
'EN_CORE_COL_CAL_LIN_5':    "15",

'EN_CORE_COL_CAL_SYNC_1': "65535",
'EN_CORE_COL_CAL_SYNC_2': "65535",
'EN_CORE_COL_CAL_SYNC_3': "65535",
'EN_CORE_COL_CAL_SYNC_4': "65535",

'EN_CORE_COL_CAL_DIFF_1': "65535",
'EN_CORE_COL_CAL_DIFF_2': "65535",
'EN_CORE_COL_CAL_DIFF_3': "65535",
'EN_CORE_COL_CAL_DIFF_4': "65535",
'EN_CORE_COL_CAL_DIFF_5':    "15",

'HITOR_0_MASK_LIN_0':     "0",
'HITOR_0_MASK_LIN_1':     "0",
'HITOR_1_MASK_LIN_0':     "0",
'HITOR_1_MASK_LIN_1':     "0",
'HITOR_2_MASK_LIN_0':     "0",
'HITOR_2_MASK_LIN_1':     "0",
'HITOR_3_MASK_LIN_0':     "0",
'HITOR_3_MASK_LIN_1':     "0",

'HITOR_0_MASK_SYNC': "65535",
'HITOR_1_MASK_SYNC': "65535",
'HITOR_2_MASK_SYNC': "65535",
'HITOR_3_MASK_SYNC': "65535",

'HITOR_0_MASK_DIFF_0': "65535",
'HITOR_0_MASK_DIFF_1'    :     "1",
'HITOR_1_MASK_DIFF_0'    : "65535",
'HITOR_1_MASK_DIFF_1'    :     "1",
'HITOR_2_MASK_DIFF_0'    : "65535",
'HITOR_2_MASK_DIFF_1'    :     "1",
'HITOR_3_MASK_DIFF_0'    : "65535",
'HITOR_3_MASK_DIFF_1'    :     "1",

'LOCKLOSS_CNT'           :     "0",
'BITFLIP_WNG_CNT'        :     "0",
'BITFLIP_ERR_CNT'        :     "0",
'CMDERR_CNT'             :     "0",
'SKIPPED_TRIGGER_CNT'    :     "0",
'HITOR_0_CNT'            :     "0",
'HITOR_1_CNT'            :     "0",
'HITOR_2_CNT' : "0",
'HITOR_3_CNT' : "0",
"SEL_CAL_RANGE":  "0",

'WNGFIFO_FULL_CNT_0':  "0",
'WNGFIFO_FULL_CNT_1':  "0",
'WNGFIFO_FULL_CNT_2':  "0",
'WNGFIFO_FULL_CNT_3':  "0",

}

globalSettingsB = {
    "EN_CORE_COL_CAL_0"    :     "65535",
    "EN_CORE_COL_CAL_1"    :     "65535",
    "EN_CORE_COL_CAL_2"    :     "65535",
    "EN_CORE_COL_CAL_3"    :        "63",

    "HITOR_MASK_0"          :    "65535",
    "HITOR_MASK_1"          :    "65535",
    "HITOR_MASK_2"          :    "65535",
    "HITOR_MASK_3"          :       "63",

    "PrecisionToTEnable_0"   :       "0",
    "PrecisionToTEnable_1"   :       "0",
    "PrecisionToTEnable_2"   :       "0",
    "PrecisionToTEnable_3"   :       "0",

    "EnHitsRemoval_0"         :      "0",
    "EnHitsRemoval_1"         :      "0",
    "EnHitsRemoval_2"         :      "0",
    "EnHitsRemoval_3"         :      "0",

    "EnIsolatedHitRemoval_0"   :     "0",
    "EnIsolatedHitRemoval_1"   :     "0",
    "EnIsolatedHitRemoval_2"   :     "0",
    "EnIsolatedHitRemoval_3"   :     "0",

    "HIT_SAMPLE_MODE"          :    "1",
    "EN_SEU_COUNT"             :    "0",
    "CDR_CONFIG_SEL_PD"        :    "0",

    "LOCKLOSS_CNT":  "0",
    "BITFLIP_WNG_CNT":  "0",
    "BITFLIP_ERR_CNT":  "0",
    "CMDERR_CNT":  "0",
    "SKIPPED_TRIGGER_CNT":  "0",
    "HITOR_0_CNT":  "0",
    "HITOR_1_CNT":  "0",
    "HITOR_2_CNT":  "0",
    "HITOR_3_CNT":  "0",
    
    "HitOrPatternLUT":  "0xFFFE",

    "READTRIG_CNT"              :   "0",
    "RDWRFIFOERROR_CNT"         :   "0",
    "PIXELSEU_CNT"              :   "0",
    "GLOBALCONFIGSEU_CNT"       :   "0",
    
    "SEL_CAL_RANGE"             :   "0",
}

globalSettingsB_Gain = copy.deepcopy(globalSettingsB)
globalSettingsB_Gain["SEL_CAL_RANGE"] = "1"
 
globalSettingsB_PixelAlive_coupled = copy.deepcopy(globalSettingsB)
globalSettingsB_PixelAlive_coupled["SEL_CAL_RANGE"] = "1"
globalSettingsB_PixelAlive_coupled["VCAL_HIGH"] = "4000"
globalSettingsB_PixelAlive_coupled["CAL_EDGE_FINE_DELAY"] = "28"
 
globalSettingsB_PixelAlive_uncoupled= copy.deepcopy(globalSettingsB)
globalSettingsB_PixelAlive_uncoupled["SEL_CAL_RANGE"] = "1"
globalSettingsB_PixelAlive_uncoupled["VCAL_HIGH"] = "4000"
globalSettingsB_PixelAlive_uncoupled["CAL_EDGE_FINE_DELAY"] = "28"
 
globalSettingsB_GenDACDAC = copy.deepcopy(globalSettingsB)
globalSettingsB_GenDACDAC["SEL_CAL_RANGE"] = "1"
globalSettingsB_GenDACDAC["VCAL_HIGH"] = "4000"

globalSettingsB_xtalk = copy.deepcopy(globalSettingsB)
globalSettingsB_xtalk["SEL_CAL_RANGE"] = "1"
globalSettingsB_xtalk["VCAL_HIGH"] = "4000"

globalSettings_DictA = {}
for key in HWSettings.HWSettings_DictA:
    globalSettings_DictA[key] = globalSettingsA

globalSettings_DictB = {
    "GainScan" : globalSettingsB_Gain,
    "GenericDAC-DAC_coupled" : globalSettingsB_xtalk,
    "GenericDAC-DAC_uncoupled" : globalSettingsB_xtalk,
    "PixelAlive_coupled" : globalSettingsB_xtalk,
    "PixelAlive_uncoupled" : globalSettingsB_xtalk,
    "SCurve_decoupled" : globalSettingsB_xtalk,
    "SCurve_coupled" : globalSettingsB_xtalk,
    "SCurve_direct" : globalSettingsB_xtalk,
}
for key in HWSettings.HWSettings_DictB:
    if key not in globalSettings_DictB:
        globalSettings_DictB[key] = globalSettingsB
