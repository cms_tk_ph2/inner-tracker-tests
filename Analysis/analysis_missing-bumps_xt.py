##############################################################################
# Author: Chin-Chia Kuo (chin-chia.kuo@cern.ch)
# Date: 17/10/2024
# Input: 2 pixelalive root files from pixelalive scans (inj = 5 and 6) and 1 txt file after masking of noise scan and pixelalive scan (inj = 1)
# Usage: python3 analysis_missing-bumps_xt.py -chipID 15 -pixelalive_inj5 Run000015 -pixelalive_inj6 Run000017 -mask Run000011 -dut_type scc
# Output: pdf plots with the main results
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##############################################################################

import ROOT; ROOT.gErrorIgnoreLevel = ROOT.kWarning; ROOT.gROOT.SetBatch(True) 
import numpy as np

import os
import re
import sys
import matplotlib
import matplotlib.pyplot as plt
import argparse

# Arguments --------------------

parser = argparse.ArgumentParser(description='Do the crosstalk analysis')
parser.add_argument('-hybridID','--hybridID', help = 'The Hybrid_ID', default = 0, type = int)
parser.add_argument('-chipID','--chipID', help = 'The Chip_ID', default = 15, type = int)
parser.add_argument('-pixelalive_inj5','--pixelalive_inj5', help = 'The name of the PixelAlive.root file', default = 'Run000015', type = str)
parser.add_argument('-pixelalive_inj6','--pixelalive_inj6', help = 'The name of the PixelAlive.root file', default = 'Run000017', type = str)
parser.add_argument('-mask','--mask', help = 'The name of the mask txt file', default = 'Run000011', type = str)
parser.add_argument('-inj5_effthr_low','--inj5_effthr_low', help = 'The low threshold of PixelAlive efficiency', default = 0.1, type = float)
parser.add_argument('-inj5_effthr_high','--inj5_effthr_high', help = 'The high threshold of PixelAlive efficiency', default = 0.98, type = float)
parser.add_argument('-inj6_effthr_low','--inj6_effthr_low', help = 'The low threshold of PixelAlive efficiency', default = 0.1, type = float)
parser.add_argument('-inj6_effthr_high','--inj6_effthr_high', help = 'The high threshold of PixelAlive efficiency', default = 0.98, type = float)
parser.add_argument('-dut_type','--dut_type', help  = 'DUT: quad or scc', default = 'scc', type = str)
args = parser.parse_args()

data_folder_path = 'Results/'
output_folder_path = 'Output/'

if args.dut_type == 'scc': analyzed_txt_file = data_folder_path + args.mask + '_CMSIT_RD53B.txt'
elif args.dut_type == 'quad': analyzed_txt_file = data_folder_path + args.mask + '_CMSIT_RD53B_' + str(int(args.chipID)) +'.txt'


# Functions --------------------

def Plots(missing_map):
    # MISSING BUMPS FINAL MAPS
    fig1 = plt.figure(figsize=(10500/300, 7500/300), dpi=300)
    plt.rcParams.update({'font.size': 27})
    ax = fig1.add_subplot(111)
    ax.spines["bottom"].set_linewidth(1); ax.spines["left"].set_linewidth(1); ax.spines["top"].set_linewidth(1); ax.spines["right"].set_linewidth(1)
    bounds = [-15, -5, 5, 15, 25]
    cmap = matplotlib.colors.ListedColormap(['blue','white','orange','red'])
    norm =matplotlib.colors.BoundaryNorm(bounds, cmap.N)
    imgplot2 = ax.imshow(missing_map.T - 1,cmap=cmap,norm=norm)
    ax.set_title('Missing Map with Crosstalk Method', fontsize = 50)
    ax.set_aspect(0.25)
    bar2=plt.colorbar(imgplot2, ticks=bounds, orientation='horizontal', label='             Masked                             Good                            Problematic                         Missing         ',  spacing='proportional', shrink=0.7)
    bar2.set_ticks([])
    fig1.savefig(output_folder_path + 'Chip' + str(int(C_ID)) + '/Missingmap_Chip' + str(int(C_ID)) + '.pdf', format='pdf', dpi=300, bbox_inches='tight')
    return


def Plots2(input_array, output_file_name, step, effthr_low, effthr_high, title):
    # 1D EFFICIENCY DISTRIBUTION
    input_array_L = input_array.flatten() * 100
    fig2 = plt.figure(figsize=(1050/96, 750/96), dpi=96)
    ax = fig2.add_subplot(111)
    ax.spines["bottom"].set_linewidth(1); ax.spines["left"].set_linewidth(1); ax.spines["top"].set_linewidth(1); ax.spines["right"].set_linewidth(1)
    ax.set_yscale('log')
    h_LIN=plt.hist(input_array_L,color='black',bins = range(0,int(102),step),label='Total: ' + str(num_cols * num_rows),histtype='step')
    ax.plot([effthr_low * 100, effthr_low * 100], [0,2e3], '--', color='red', linewidth=3)
    ax.plot([effthr_high * 100, effthr_high * 100], [0,2e3], '--', color='orange', linewidth=3)
    ax.set_title(title)
    ax.set_xlabel('Efficiency [%]')
    ax.set_ylabel('entries')
    ax.legend(prop={'size': 14}, loc='upper left')
    fig2.savefig(output_folder_path + 'Chip' + str(int(args.chipID)) + '/' + output_file_name + '_Chip' + str(int(C_ID)) + '.pdf', format='pdf', dpi=300)
    return


def Plots3(input_array, output_file_name, title):
    # 2D EFFICIENCY MAPS
    fig3 = plt.figure(figsize=(10500/300, 7500/300), dpi=300)
    plt.rcParams.update({'font.size': 27})
    ax = fig3.add_subplot(111)
    ax.spines["bottom"].set_linewidth(1); ax.spines["left"].set_linewidth(1); ax.spines["top"].set_linewidth(1); ax.spines["right"].set_linewidth(1)
    imgplot2 = ax.imshow(input_array.T)
    ax.set_title(title, fontsize = 50)
    bar2 = plt.colorbar(imgplot2, orientation='vertical')
    ax.set_aspect(0.25)
    fig3.savefig(output_folder_path + 'Chip' + str(int(C_ID)) + '/' + output_file_name + '_Chip' + str(int(C_ID)) + '.pdf', format='pdf', dpi=300, bbox_inches='tight')
    return


def Get_Efficiency_Map(infile, eff_thr_low, eff_thr_high, num_rows, num_cols, H_ID, C_ID):
    eff_map = np.zeros((num_cols, num_rows))
    eff_raw_map = np.zeros((num_cols, num_rows))
    bad_no = 0
    canvas = infile.Get("Detector/Board_0/OpticalGroup_0/Hybrid_"+str(int(H_ID))+"/Chip_"+str(int(C_ID))+"/D_B(0)_O(0)_H("+str(int(H_ID))+")_PixelAlive_Chip("+str(int(C_ID))+")")
    eff_map_r = canvas.GetPrimitive("D_B(0)_O(0)_H("+str(int(H_ID))+")_PixelAlive_Chip("+str(int(C_ID))+")")
    for i in range(1, eff_map_r.GetNbinsX() + 1):
        for j in range(1, eff_map_r.GetNbinsY() + 1):
            eff_raw_map[(i - 1),(j - 1)] = eff_map_r.GetBinContent(i, j)
            
            if (eff_map_r.GetBinContent(i, j) < eff_thr_low):
                bad_no += 1
                eff_map[(i - 1),(j - 1)] = 2

            elif (eff_map_r.GetBinContent(i, j) < eff_thr_high):
                eff_map[(i - 1),(j - 1)] = 1

    print('   -->  Bad pixels in chip-'+ str(int(C_ID)) + ' = ' + str(int(bad_no)))
    infile.Close()
    return To25x100SensorCoordinates(eff_map), To25x100SensorCoordinates(eff_raw_map)


def To25x100SensorCoordinates(npArray):
    new_rows=num_rows * 2; new_cols = num_cols // 2
    NewArray=np.zeros((new_cols,new_rows), dtype=npArray.dtype)
    for i in range(num_cols):
        for j in range(num_rows):
            #Conversion from CMSIT coverter plugin for 25x100r0c0 of Mauro
            row = 2*j+(i%2)
            col = int(i/2)
            NewArray[col,row]=npArray[i,j]
    return NewArray


def GetMaskFromTxt(file_path,num_rows,num_cols):
    array_2d = np.zeros((num_rows,num_cols))
    col=-1
    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith("COL "): col+=1
            if line.startswith("ENABLE "):
                enable_row = line.replace("ENABLE ", "").strip().split(',')
                for row,value in enumerate(enable_row): array_2d[row,col]=int(value)

    Masked = np.where(array_2d == 0)
    mask_map = np.zeros((num_cols,num_rows))
    for i,val in enumerate (Masked[0]):
         mask_map[int(Masked[1][i])][val] = -10
    return To25x100SensorCoordinates(mask_map)


# Missing-bump Analysis --------------------

num_rows = 336; num_cols = 432

H_ID=args.hybridID; C_ID=args.chipID

if not os.path.exists(output_folder_path + 'Chip' + str(int(C_ID))): os.makedirs(output_folder_path + 'Chip' + str(int(C_ID)))

#Get information from PixelAlive (INJ = 5)
print("\n ========  INJ_type: 5  ========")
inj5_file = ROOT.TFile.Open(data_folder_path + args.pixelalive_inj5 + '_PixelAlive.root',"READ")
inj5_map, inj5_eff_raw_map = Get_Efficiency_Map(inj5_file, args.inj5_effthr_low, args.inj5_effthr_high, int(num_rows), int(num_cols), H_ID, C_ID)

#Get information from PixelAlive (INJ = 6)
print("\n ========  INJ_type: 6  ========")
inj6_file = ROOT.TFile.Open(data_folder_path + args.pixelalive_inj6 + '_PixelAlive.root',"READ")
inj6_map, inj6_eff_raw_map = Get_Efficiency_Map(inj6_file, args.inj6_effthr_low, args.inj6_effthr_high, int(num_rows), int(num_cols), H_ID, C_ID)

#Merge information together
missing_map = inj5_map * 10 + inj6_map 


#Add mask information from txt
print("\n ======== Masked before ========")
mask_map = GetMaskFromTxt(analyzed_txt_file,num_rows,num_cols)

mask_no = 0
for i in range(num_cols // 2):
	for j in range(num_rows * 2):
		if (mask_map[i][j] == -10):
			mask_no += 1
		if ((mask_map[i][j] != 0)):
			missing_map[i][j] = mask_map[i][j]

print("   -->  Masked pixels in chip-" + str(int(C_ID)) + " =", mask_no, "\n")


# Summary --------------------

all_good_no = 0; all_mask_no = 0; all_problematic_no = 0; all_missing_no = 0

missing_map_color = missing_map * 0 

# For missing_map: Missing == 22; Problematic == 11 12 21; Good == 00 01 02 10 20; Masked == -10
# For missing_map_color: Missing == 20; Problematic == 10; Good == 0; Masked == -10
for i in range(num_cols // 2):
    for j in range(num_rows * 2):
        if (missing_map[i][j] == 0 or missing_map[i][j] == 1 or missing_map[i][j] == 2 or missing_map[i][j] == 10 or missing_map[i][j] == 20):
            all_good_no += 1
            missing_map_color[i][j] = 0
        elif (missing_map[i][j] == 11 or missing_map[i][j] == 12 or missing_map[i][j] == 21):
            all_problematic_no += 1
            missing_map_color[i][j] = 10
        elif (missing_map[i][j] == 22):
            all_missing_no += 1
            missing_map_color[i][j] = 20
        elif (missing_map[i][j] == -10):
            all_mask_no += 1
            missing_map_color[i][j] = -10
          
           #print('column: ', i, 'row: ', j)

# 2D missing map
Plots(missing_map_color)

# 2D efficiency maps
Plots3(inj5_eff_raw_map, 'inj5_eff_2D', 'Efficiency map with INJtype = 5')
Plots3(inj6_eff_raw_map, 'inj6_eff_2D', 'Efficiency map with INJtype = 6')

# 1D efficiency distribution plots
Plots2(inj5_eff_raw_map, 'inj5_eff_1D', 2, args.inj5_effthr_low, args.inj5_effthr_high, '1D Efficiency with INJtype = 5')
Plots2(inj6_eff_raw_map, 'inj6_eff_1D', 2, args.inj6_effthr_low, args.inj6_effthr_high, '1D Efficiency with INJtype = 6')

print('==================================================================================\n INFO\n==================================================================================')
print('Masked_pixel:\t\t'+str(all_mask_no))
print('Missing:\t\t'+str(all_missing_no)+'\t\t\tProblematic:\t\t'+str(all_problematic_no))
print('Sum is: \t\t'+str(all_good_no + all_mask_no + all_problematic_no + all_missing_no)+'\t\t\tTotal = of pixels:\t'+str(num_cols * num_rows))
print('==================================================================================\n')


