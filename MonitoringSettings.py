import sys
import os
import copy

hw_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(hw_dir)
import HWSettings

MonitoringListA = {
  'VIN_ana_ShuLDO'  : "0",
  'VOUT_ana_ShuLDO' : "0",
  'VIN_dig_ShuLDO'  : "0",
  'VOUT_dig_ShuLDO' : "0",
  'ADCbandgap'      : "0",
  'Iref'            : "0",
  'TEMPSENS_1'      : "0",
  'TEMPSENS_4'      : "1",
}

MonitoringListB = {
  'VINA'            : "0",
  'VDDA'            : "0",
  'ANA_IN_CURR'     : "0",
  'VIND'            : "0",
  'VDDD'            : "0",
  'DIG_IN_CURR'     : "0",
  'Iref'            : "0",
  'POLY_TEMP_SENS_TOP' : "0",
  'POLY_TEMP_SENS_BOTTOM' : "0",
  'TEMPSENS_ANA_SLDO' : "0",
  'TEMPSENS_DIG_SLDO' : "0",
  'TEMPSENS_CENTER' : "0",
  'INTERNAL_NTC'    : "1",
}

Monitoring_DictB = {}

Monitoring_DictB['SLDOScan_GADC'] = {
  'VINA'            : "0",
  'VDDA'            : "1",
  'ANA_IN_CURR'     : "0",
  'VIND'            : "0",
  'VDDD'            : "1",
  'DIG_IN_CURR'     : "0",
  'Iref'            : "0",
  'POLY_TEMP_SENS_TOP' : "0",
  'POLY_TEMP_SENS_BOTTOM' : "0",
  'TEMPSENS_ANA_SLDO' : "0",
  'TEMPSENS_DIG_SLDO' : "0",
  'TEMPSENS_CENTER' : "0",
  'INTERNAL_NTC'    : "1",
}

for key in HWSettings.HWSettings_DictB:
    if key not in Monitoring_DictB:
        Monitoring_DictB[key] = MonitoringListB