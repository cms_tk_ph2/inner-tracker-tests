import array
import ROOT
import csv
import re

def getIVCurve(csvFilename, row1, row2):
  v_I = []
  v_V = []
  file = open(csvFilename)
  reader = csv.reader(file)
  rows = list(reader)
  v_I = [float(value) for value in rows[row1]]; v_I = array.array('d', v_I)
  v_V = [float(value) for value in rows[row2]]; v_V = array.array('d', v_V)
  return (v_I, v_V)

def makeSLDOPlot(moduleName,chipFilename, str_DigiAna, str_Chip):
  (v_I_rampUp, v_Vin_rampUp) = getIVCurve(chipFilename, 0, 1)
  (v_I_rampUp, v_VDD_rampUp) = getIVCurve(chipFilename, 0, 2)
  (v_I_rampDn, v_Vin_rampDn) = getIVCurve(chipFilename, 3, 4)
  (v_I_rampDn, v_VDD_rampDn) = getIVCurve(chipFilename, 3, 5)

  g_Vin_rampUp = ROOT.TGraph(len(v_I_rampUp), v_I_rampUp, v_Vin_rampUp)
  g_VDD_rampUp = ROOT.TGraph(len(v_I_rampUp), v_I_rampUp, v_VDD_rampUp)
  g_Vin_rampDn = ROOT.TGraph(len(v_I_rampDn), v_I_rampDn, v_Vin_rampDn)
  g_VDD_rampDn = ROOT.TGraph(len(v_I_rampDn), v_I_rampDn, v_VDD_rampDn)

  # Customize graph appearance
  g_Vin_rampUp.SetMarkerStyle(20); g_Vin_rampUp.SetLineColor(ROOT.kBlue); g_Vin_rampUp.SetMarkerColor(ROOT.kBlue)
  g_Vin_rampDn.SetMarkerStyle(20); g_Vin_rampDn.SetLineColor(ROOT.kGreen+2); g_Vin_rampDn.SetMarkerColor(ROOT.kGreen+2)
  g_VDD_rampUp.SetMarkerStyle(20); g_VDD_rampUp.SetLineColor(ROOT.kRed); g_VDD_rampUp.SetMarkerColor(ROOT.kRed)
  g_VDD_rampDn.SetMarkerStyle(20); g_VDD_rampDn.SetLineColor(ROOT.kMagenta); g_VDD_rampDn.SetMarkerColor(ROOT.kMagenta)

  g_Vin_rampUp.SetTitle(moduleName+" Chip "+str_Chip+" "+str_DigiAna+"; Current (A); Voltage (V)")

  g_Vin_rampUp.SetName("g_Vin_"+str_DigiAna+"_rampUp")
  g_Vin_rampDn.SetName("g_Vin_"+str_DigiAna+"_rampDn")
  g_VDD_rampUp.SetName("g_VDD_"+str_DigiAna+"_rampUp")
  g_VDD_rampDn.SetName("g_VDD_"+str_DigiAna+"_rampDn")

  c_Chip = ROOT.TCanvas(str_DigiAna, str_DigiAna, 800, 800)
  legend = ROOT.TLegend(0.15, 0.89, 0.5, 0.7)
  legend.AddEntry(g_Vin_rampUp, "V_{in} ramp up")
  legend.AddEntry(g_Vin_rampDn, "V_{in} ramp down")
  legend.AddEntry(g_VDD_rampUp, "VDD ramp up")
  legend.AddEntry(g_VDD_rampDn, "VDD ramp down")
  legend.SetLineColor(0); legend.SetFillColor(0)
  g_Vin_rampUp.Draw("APL")
  g_Vin_rampDn.Draw("PL")
  g_VDD_rampUp.Draw("PL")
  g_VDD_rampDn.Draw("PL")
  legend.Draw()
  c_Chip.Update()
  c_Chip.Write(str_DigiAna+"_Chip("+str_Chip+")")

def SLDO_CSV_to_ROOT(moduleName, moduleCanvasPath, list_csvFiles, outputDir):
  file = ROOT.TFile(outputDir+"/Result_RunSLDO_"+moduleName+".root", "RECREATE")

  # Find out Optical Board, Group, Hybrid
  match_canvasPath = re.search(r"Board_(\d+).*OpticalGroup_(\d+).*Hybrid_(\d+)", moduleCanvasPath)
  n_board = match_canvasPath.group(1)
  n_group = match_canvasPath.group(2)
  n_hybrid = match_canvasPath.group(3)
  name_canvas = "D_B("+n_board+")_O("+n_group+")_H("+n_hybrid+")_SLDO_"

  for csvFile in list_csvFiles:
    match = re.search(r"VDD([AD]).*ROC(\d+)\.csv", csvFile)
    if match:
      vdd_type = match.group(1)
      roc_num = match.group(2)
      if not file.GetDirectory(moduleCanvasPath+"/Chip_"+roc_num):
        file.mkdir(moduleCanvasPath+"/Chip_"+roc_num)
      file.cd(moduleCanvasPath+"/Chip_"+roc_num)
      if vdd_type == "A":
        makeSLDOPlot(moduleName, csvFile, name_canvas+"Analog", roc_num)
      elif vdd_type == "D":
        makeSLDOPlot(moduleName, csvFile, name_canvas+"Digital", roc_num)
  file.Close()
