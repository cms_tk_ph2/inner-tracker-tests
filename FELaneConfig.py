import sys
import os
import copy
import HWSettings

hw_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(hw_dir)

FESingleLaneConfigB = {
    "isPrimary": "1",
    "masterLane": "0",
    "outputLanes": "0001",
    "singleChannelInputs": "0000",
    "dualChannelInput": "0000",
}

FELaneConfigB_DataMergingQuick_MMMM = [
    copy.deepcopy(FESingleLaneConfigB) for i in range(4)
]

FELaneConfigB_DataMergingQuick_MMMM_TBPX = copy.deepcopy(FELaneConfigB_DataMergingQuick_MMMM)
FELaneConfigB_DataMergingQuick_M111_TBPX = copy.deepcopy(FELaneConfigB_DataMergingQuick_MMMM)
FELaneConfigB_DataMergingQuick_M111_TBPX[0]["singleChannelInputs"] = "1101"
FELaneConfigB_DataMergingQuick_M111_TBPX[1]["isPrimary"] = "0"
FELaneConfigB_DataMergingQuick_M111_TBPX[1]["outputLanes"] = "0010"
FELaneConfigB_DataMergingQuick_M111_TBPX[2]["isPrimary"] = "0"
FELaneConfigB_DataMergingQuick_M111_TBPX[2]["outputLanes"] = "0010"
FELaneConfigB_DataMergingQuick_M111_TBPX[3]["isPrimary"] = "0"
FELaneConfigB_DataMergingQuick_M111_TBPX[3]["outputLanes"] = "0010"

FELaneConfigB_DataMergingQuick_M2M2_TBPX = copy.deepcopy(FELaneConfigB_DataMergingQuick_MMMM)
FELaneConfigB_DataMergingQuick_M2M2_TBPX[0]["dualChannelInput"] = "0011"
FELaneConfigB_DataMergingQuick_M2M2_TBPX[1]["isPrimary"] = "0"
FELaneConfigB_DataMergingQuick_M2M2_TBPX[1]["outputLanes"] = "0210"
FELaneConfigB_DataMergingQuick_M2M2_TBPX[2]["dualChannelInput"] = "0011"
FELaneConfigB_DataMergingQuick_M2M2_TBPX[3]["isPrimary"] = "0"
FELaneConfigB_DataMergingQuick_M2M2_TBPX[3]["masterLane"] = "2"
FELaneConfigB_DataMergingQuick_M2M2_TBPX[3]["outputLanes"] = "1200"

FELaneConfigB_DataMergingQuick_M2M2_TFPX = copy.deepcopy(FELaneConfigB_DataMergingQuick_MMMM)
FELaneConfigB_DataMergingQuick_M2M2_TFPX[2]["dualChannelInput"] = "0011"
FELaneConfigB_DataMergingQuick_M2M2_TFPX[1]["isPrimary"] = "0"
FELaneConfigB_DataMergingQuick_M2M2_TFPX[1]["masterLane"] = "2"
FELaneConfigB_DataMergingQuick_M2M2_TFPX[1]["outputLanes"] = "0120"
FELaneConfigB_DataMergingQuick_M2M2_TFPX[0]["dualChannelInput"] = "0011"
FELaneConfigB_DataMergingQuick_M2M2_TFPX[3]["isPrimary"] = "0"
FELaneConfigB_DataMergingQuick_M2M2_TFPX[3]["masterLane"] = "0"
FELaneConfigB_DataMergingQuick_M2M2_TFPX[3]["outputLanes"] = "0012"


FELaneConfigB_DataMergingQuick_MMMM_TFPX = copy.deepcopy(FELaneConfigB_DataMergingQuick_MMMM)
FELaneConfigB_DataMergingQuick_MMMM_TFPX[2]["outputLanes"] = "0001"
FELaneConfigB_DataMergingQuick_MMMM_TFPX[1]["outputLanes"] = "0100"
FELaneConfigB_DataMergingQuick_MMMM_TFPX[0]["outputLanes"] = "0001"
FELaneConfigB_DataMergingQuick_MMMM_TFPX[3]["outputLanes"] = "0100"


FELaneConfig_DictB = {}
FELaneConfig_DictB['TBPX'] = {
    "DataMergingQuick_MMMM_TBPX": FELaneConfigB_DataMergingQuick_MMMM_TBPX,
    "DataMergingQuick_M111_TBPX": FELaneConfigB_DataMergingQuick_M111_TBPX,
    "DataMergingQuick_M2M2_TBPX": FELaneConfigB_DataMergingQuick_M2M2_TBPX,
    }

FELaneConfig_DictB['TEPX'] = {
    "DataMergingQuick_MMMM_TBPX": FELaneConfigB_DataMergingQuick_MMMM_TBPX,
    "DataMergingQuick_M111_TBPX": FELaneConfigB_DataMergingQuick_M111_TBPX,
    "DataMergingQuick_M2M2_TBPX": FELaneConfigB_DataMergingQuick_M2M2_TBPX,
    }

FELaneConfig_DictB['TFPX'] = {
    "DataMergingQuick_MMMM_TFPX": FELaneConfigB_DataMergingQuick_MMMM_TFPX,
    "DataMergingQuick_M2M2_TFPX": FELaneConfigB_DataMergingQuick_M2M2_TFPX,
    }

FELaneConfig_DictB['CROC'] = {
    "DataMergingQuick_MMMM_TFPX ": FELaneConfigB_DataMergingQuick_MMMM_TFPX,
    }

for key in HWSettings.HWSettings_DictB:
    if key not in FELaneConfig_DictB['TBPX'].keys():
        FELaneConfig_DictB['TBPX'][key] = FELaneConfigB_DataMergingQuick_MMMM_TBPX
    if key not in FELaneConfig_DictB['TEPX'].keys():
        FELaneConfig_DictB['TEPX'][key] = FELaneConfigB_DataMergingQuick_MMMM_TBPX
    if key not in FELaneConfig_DictB['TFPX'].keys():
        FELaneConfig_DictB['TFPX'][key] = FELaneConfigB_DataMergingQuick_MMMM_TFPX
    if key not in FELaneConfig_DictB['CROC'].keys():
        FELaneConfig_DictB['CROC'][key] = FELaneConfigB_DataMergingQuick_MMMM_TFPX  #This is just for Single chip cards.  Need to make this more obvious/better.
